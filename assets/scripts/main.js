$(document).ready(function() {
  $("#show-documents").click(function(e) {
    e.preventDefault();
    $("#document-links")
      .stop(true, true)
      .slideToggle()
      .toggleClass("shown");
    if ($("#document-links").hasClass("shown")) {
      $("#show-documents .chevron").addClass("rotated");
    } else {
      $("#show-documents .chevron").removeClass("rotated");
    }
  });

  var city = $("main").attr("data-city");

  $("[data-map-city='"+ city + "']").addClass('active');
  

  $("#menu a").click(function(e) {
    e.preventDefault();
    $("#menu").removeClass("visible");
    $("html, body").removeClass("noscroll");
    $("#hamburger").removeClass("open");
  });

  $("#hamburger").click(function(e) {
    $("#menu").toggleClass("visible");
    if ($("#menu").hasClass("visible")) {
      $("html, body").addClass("noscroll");
      $("#hamburger").addClass("open");
    } else {
      $("html, body").removeClass("noscroll");
      $("#hamburger").removeClass("open");
    }
  });

  var scrollMagicController = new ScrollMagic.Controller();

  $('[data-scrollmagic="slide-up"]').each(function(index, element) {
    new ScrollMagic.Scene({ triggerElement: element, triggerHook: 0.7 })
      .setClassToggle(element, "active")
      .addTo(scrollMagicController);
  });
  $('[data-scrollmagic="slide-down"]').each(function(index, element) {
    new ScrollMagic.Scene({ triggerElement: element, triggerHook: 0.7 })
      .setClassToggle(element, "active")
      .addTo(scrollMagicController);
  });

  setTimeout(function() {
    $('[data-scrollmagic="scale-up"]').each(function(index, element) {
      new ScrollMagic.Scene({ triggerElement: element, triggerHook: 0.9 })
        .setClassToggle(element, "active-city")
        .addTo(scrollMagicController);
    });
  }, 300);

  $("[data-scrolltoonclick]").click(function(e) {
    e.preventDefault();
    var $target = $($(this).attr("href"));
    if ($target.length) {
      $("html, body").animate(
        {
          scrollTop: $target.offset().top
        },
        500
      );
    }
  });
});
