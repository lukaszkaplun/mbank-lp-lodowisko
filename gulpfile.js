var gulp = require("gulp"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  autoprefixer = require("autoprefixer"),
  cssnano = require("cssnano"),
  sourcemaps = require("gulp-sourcemaps"),
  browserSync = require("browser-sync").create(),
  handlebars = require("gulp-compile-handlebars"),
  rename = require("gulp-rename"),
  uglify = require("gulp-uglify"),
  concat = require("gulp-concat"),
  htmlreplace = require("gulp-html-replace"),
  cities = require("./assets/scripts/cities.json");

function style(done) {
  gulp
    .src("./assets/css/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./assets/css/"))
    // Add browsersync stream pipe after compilation
    .pipe(browserSync.stream());

  done();
}
function scripts(done) {
  gulp.src("./assets/scripts/*.js").pipe(browserSync.stream());

  done();
}

function build(done) {
  for (var i = 0; i < cities.length; i++) {
    var city = cities[i],
      fileName = city.slug.toLowerCase();

    var data = {
      currentCity: city,
      allCities: cities
    };

    gulp
      .src([
        "./assets/css/bootstrap-reboot.min.css",
        "./assets/css/bootstrap.min.css",
        "./assets/css/jquery-ui.min.css",
        "./assets/css/main.css"
      ])
      .pipe(concat("styles.min.css"))
      .pipe(postcss([cssnano()]))

      .pipe(gulp.dest("build/" + fileName + "/assets/css/"));
    gulp
      .src(
        [
          "./assets/img/*",
          "!./assets/img/coat-*",
          "./assets/img/coat-" + city.slug + ".*"
        ],
        { allowEmpty: true }
      )
      .pipe(gulp.dest("build/" + fileName + "/assets/img/"));
    gulp
      .src([
        "./assets/scripts/jquery.min.js",
        "./assets/scripts/bootstrap.bundle.min.js",
        "./assets/scripts/scrollmagic.min.js",
        "./assets/scripts/tweenmax.min.js",
        "./assets/scripts/animation.gsap.min.js",
        "./assets/scripts/main.js",
        "!./assets/scripts/*.json"
      ])
      .pipe(concat("bundle.min.js"))
      .pipe(uglify())
      .pipe(gulp.dest("build/" + fileName + "/assets/scripts/"));
    gulp
      .src("./assets/fonts/*")
      .pipe(gulp.dest("build/" + fileName + "/assets/fonts/"));

    gulp
      .src("templates/*.handlebars")
      .pipe(handlebars(data))
      .pipe(rename("index.html"))
      .pipe(
        htmlreplace({
          css: "./assets/css/styles.min.css",
          js: "./assets/scripts/bundle.min.js"
        })
      )
      .pipe(gulp.dest("build/" + fileName));
  }
  done();
}

function handlebarsIndex(done) {
  var data = {
    currentCity: cities[8],
    allCities: cities
  };

  gulp
    .src("./templates/index.handlebars")
    .pipe(handlebars(data))
    .pipe(rename("index.html"))
    .pipe(gulp.dest("./"));

  done();
}

function watch(done) {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  gulp.watch("./assets/css/*.scss", style);
  gulp.watch("./assets/scripts/*.js", scripts);
  gulp.watch("./templates/index.handlebars", handlebarsIndex);
  gulp.watch("./*.html").on("change", browserSync.reload);
  gulp.watch("./js/*.js").on("change", browserSync.reload);

  done();
}

// Don't forget to expose the task!
exports.style = style;
exports.watch = watch;
exports.build = build;
exports.handlebarsIndex = handlebarsIndex;
